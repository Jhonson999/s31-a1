// contain all business logic

const Task = require('../models/taskSchema')

// Get All Tasks
module.exports.getAllTasks = () => {
	return Task.find({}).then(result => {
		return result
	})
}


// Creating New Task

module.exports.createTask = (reqBody) => {

	let newTask = new Task({
		name: reqBody.name
	})

	return newTask.save().then((task, err) => {
		if(err){
			console.log(err)
			return false
		} else {
			return task
		}
	})
}

// Delete task

module.exports.deleteTask = (taskId) => {

	return Task.findByIdAndDelete(taskId).then((removedTask, err) => {

		if(err){
			console.log(err)
			return false
		} else {

			return removedTask
		}
	})
}

// update task controller

module.exports.updateTask = (taskId, newContent) => {

	return Task.findById(taskId).then((result, err) => {

		if(err){
			console.log(err)
			return false
		}

		result.name = newContent.name
			return result.save().then((updatedTask, saveErr) => {

				if(saveErr){
					console.log(saveErr)
					return false
				} else {

					return updatedTask
				}
			})
	})
}

// FOR ACTIVITY
//2. CREATE A CONTROLLER FUNCTION FOR RETRIEVING A SPECIFIC TASK.
//3. RETURN THE RESULT BACK TO THE CLIENT/POSTMAN.
module.exports.getOneTask = (taskId) => {
	return Task.findById(taskId).then(result => {
				return result

	})
}
//6. CREATE A CONTROLLER FUNCTION FOR CHANGING THE STATUS OF A TASK TO "complete".
//7. RETURN THE RESULT BACK TO THE CLIENT/POSTMAN.
module.exports.updateTaskToComplete = (taskId, newStatus) => {

	return Task.findById(taskId).then((result, err) => {

		if(err){
			console.log(err)
			return false
		}

		result.status = "complete"
		
	
			return result.save().then((updatedTask, saveErr) => {

				if(saveErr){
					console.log(saveErr)
					return false
				} else {

					return updatedTask
				}
			})
	})
}
