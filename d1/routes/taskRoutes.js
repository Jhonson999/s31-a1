// Contain all task endpoints for our applications

const express = require('express')
const router = express.Router()
const taskController = require('../controllers/taskControllers')

// route for getting all tasks
router.get('/', (req, res) => {

	taskController.getAllTasks().then(resultFromController => res.send(resultFromController))
})

// route for creating task

router.post('/createTask', (req, res) => {

	taskController.createTask(req.body).then(resultFromController => res.send(resultFromController))
})

// route for deleting task

router.delete('/deleteTask/:id', (req, res) => {

	taskController.deleteTask(req.params.id).then(resultFromController => res.send(resultFromController))
})

// route for updating task

router.put('/updateTask/:id', (req, res) => {

	taskController.updateTask(req.params.id, req.body).then(resultFromController => res.send(resultFromController))
})



// FOR ACTIVITY

//1. CREATE A ROUTE FOR GETTING A SPECIFIC TASK.
//4. PROCESS A GET REQUEST AT THE "/tasks/:id" ROUTE USING POSTMAN TO GET A SPECIFIC TASK.
router.get('/:id', (req, res) => {

	taskController.getOneTask(req.params.id).then(resultFromController => res.send(resultFromController))
})

//5. CREATE A ROUTE FOR CHANGING THE STATUS OF A TASK TO "complete".
//8. PROCESS A PUT REQUEST AT THE "/tasks/:id/complete" ROUTE USING POSTMAN TO UPDATE A TASK.
router.put('/:id/complete', (req, res) => {

	taskController.updateTaskToComplete(req.params.id, req.body).then(resultFromController => res.send(resultFromController))
})

module.exports = router